const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let avatar = message.author.avatarURL;
    let regdate = message.author.createdAt;
    let tagnick = message.author.tag;

    let EmbedMessage = new Discord.RichEmbed()
    .addField('유저 이름', tagnick, true)
    .addField('계정 생성 날짜', regdate)
    .setThumbnail(avatar)
    .setTitle('정보');
    
    message.channel.sendMessage(EmbedMessage);
};

module.exports.help = {
    name:"정보",
    lore: "유저 정보를 보여준다"
  }