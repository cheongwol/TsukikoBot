const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let sicon = message.guild.iconURL;
    let serverembed = new Discord.RichEmbed()
    .setDescription(`${message.member.tag}님이 요청하신 서버정보`)
    .setColor("#15f153")
    .setThumbnail(sicon)
    .addField("서버 이름", message.guild.name)
    .addField("제작된 날", message.guild.createdAt)
    .addField("들어온 날", message.member.joinedAt)
    .addField("총 사람수", message.guild.memberCount);

    message.channel.send(serverembed);
}

module.exports.help = {
  name:"서버정보"
}