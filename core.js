const config = require('./config.json');
const Discord = require('discord.js');
const client = new Discord.Client({disableEveryone: true});

const token = config.discord_token;

const fs = require('fs');
client.commands = new Discord.Collection();

/*
애드온 로딩(명령어)
*/

var addons_list = '';

if(config.startup_addon_search){
fs.readdir('./bot/addons/commands/', (err, files) => {
    console.log("[시도중]애드온을 검색중입니다...");
if(err) console.log(err);
let _cfile = files.filter(f=>f.split(".").pop() === "js")
if(_cfile.length <= 0) {
    console.log("[오류]추가 명령어 파일을 찾을 수 없습니다!");
    return;
} 
_cfile.forEach((f,i) =>{
    let _cfiles = require(`./bot/addons/commands/${f}`);
    console.log(`[성공]추가 명령어 ${f}가 로딩되었습니다!`);
    addons_list = addons_list+f+'\n';
    client.commands.set(_cfiles.help.name, _cfiles);
});
fs.writeFileSync("addons.txt", '\ufeff' + addons_list, {encoding: 'utf8'});
});


}else{console.log("[경고]애드온 검색이 꺼져있습니다.\n[경고]config.json에 startup_addon_search 를 true로 바꿔주세요\n[경고]추가기능을 불러오지 않습니다");}

/*
디스코드 봇 호출
*/
client.on('ready', async () => {
    console.log(`[성공]${client.user.tag}(으)로 로그인 되었습니다`);
});

client.on('message', async msg => {
    if(msg.author.bot) return;
    if(msg.channel.type === 'dm') return;

    let prefix = config.prefix;
    let msgargs = msg.content.split(' ');
    let cmd = msgargs[0];
    let args = msgargs.slice(1);
    let commandfiles = client.commands.get(cmd.slice(prefix.length));
    if(commandfiles) commandfiles.run(client,msg,args);
});

client.login(token);

//봇 홈페이지
var express = require('express');
var partials = require('express-partials');
var app = express();
var server_port = 80;
var routes = require('./website/routes');


var errorHandlers = require('./website/middleware/errorhandlers');
var log = require('./website/middleware/log');

app.use(partials());

app.set('views', __dirname+ '/website/views');
//app.set('view options', {defaultLayout: 'layout'});
app.set('view engine', 'ejs');
app.use(log.logger);
app.use(express.static(__dirname + '/website/static'));

//app.get('/', routes.index);
app.get('/account/login', routes.login);
app.get('/', routes.index);

app.post('/account/login', routes.loginProcess);
app.get('/error', function(req,res,next) {
    next(new Error('A contrived error'));
})

app.use(errorHandlers.error);
app.use(errorHandlers.notFound);

app.listen(server_port);
console.log(`[성공]서버가 ${server_port}에서 열렸습니다 `);