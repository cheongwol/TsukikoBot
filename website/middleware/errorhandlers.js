exports.error = function error(err, req, res, next) {
    console.log(err);
    res.send(500, '500, 무언가 망가졌습니다. 무슨 일을 하셧나요?')
}
exports.notFound = function notFound(req,res,next) {
    res.send(404, '404, 파일을 찾을 수 없습니다');
}